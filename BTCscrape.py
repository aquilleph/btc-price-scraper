from email.mime.text import MIMEText
from BeautifulSoup import BeautifulSoup
import unicodedata
import smtplib
import urllib2
import getpass

def BTCval():
	return float(unicodedata.normalize('NFKD', [n for n in BeautifulSoup(urllib2.urlopen("https://mtgox.com").read()).findAll('span')][0].string.split('$')[1]).encode('ascii','ignore'))

def email(emailTo, emailFrom, pword):
	msg = MIMEText('BTC exchange rate has reached $' + str(BTCval()))
	msg['Subject'] = 'BTC Alert!'
	msg['From'] = 'BTC Alerter'
	msg['To'] = emailTo

	s = smtplib.SMTP('smtp.gmail.com', 587)
	s.set_debuglevel(1) 
	s.ehlo() 
	s.starttls() 
	s.ehlo() 
	s.login(emailFrom, pword) 
	s.sendmail(msg['From'], msg['To'], msg.as_string())
	s.quit()

def  main():
	print ''
	print '========================================================='
	print 'Welcome to BTCscrape, a real time BitCoin value Notifier!'
	print ''
	
	emailTo = raw_input('Enter recepient address: ')
	if emailTo == '':
		emailTo = '2174166527@txt.att.net'

	emailFrom = raw_input('Enter sender address: ')
	if emailFrom == '':
		emailFrom = 'acsherrock@gmail.com'

	pword = getpass.getpass('Enter password: ')

	high = raw_input('Enter a high threshold to notify [200]: ')
	if high == '':
		high = 200
	else:
		high = int(high)

	low = raw_input('Enter a low threshold to notify [100]: ')
	if low == '':
		low = 100
	else:
		low = int(low)

	hasNotified = False

	while True:
		if BTCval() >= high or BTCval <= low:
			print BTCval()
			if hasNotified == False:
				email(emailTo, emailFrom, pword)
				hasNotified = True
		else:
			print '$' + str(BTCval())
			if hasNotified == True:
				hasNotified = False

if __name__ == '__main__':
	main()
